﻿using System;
using System.Collections.Generic;
using System.Device.Location;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using System.Windows.Threading;
using Windows.Networking.Proximity;
using Windows.Phone.Devices.Notification;
using Microsoft.Phone.BackgroundAudio;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Net.NetworkInformation;
using Microsoft.Phone.Shell;
using MeasurementApp.Resources;
using Microsoft.Phone.Tasks;

namespace MeasurementApp
{
    public partial class MainPage : PhoneApplicationPage
    {
        // Constructor
        public MainPage()
        {
            InitializeComponent();

            // Sample code to localize the ApplicationBar
            //BuildLocalizedApplicationBar();
        }

        private void VibrateButton_Click(object sender, RoutedEventArgs e)
        {
            VibrationDevice testVibrationDevice = VibrationDevice.GetDefault();
            testVibrationDevice.Vibrate(TimeSpan.FromMilliseconds(2000));
        }

        private async void VibrateSwitch_Click(object sender, RoutedEventArgs e)
        {
            VibrationDevice testVibrationDevice = VibrationDevice.GetDefault();
            while (VibrateSwitch.IsChecked == true)
            {
                testVibrationDevice.Vibrate(TimeSpan.FromMilliseconds(300));
                await Task.Delay(100);
            }
        }

        private void WiFiSwitch_Click(object sender, RoutedEventArgs e)
        {
            ConnectionSettingsTask connectionSettingsTask = new ConnectionSettingsTask();
            connectionSettingsTask.ConnectionSettingsType = ConnectionSettingsType.WiFi;
            connectionSettingsTask.Show();
            UpdateToggleButtons();
        }



        private void BluetoothSwitch_Click(object sender, RoutedEventArgs e)
        {
            ConnectionSettingsTask connectionSettingsTask = new ConnectionSettingsTask();
            connectionSettingsTask.ConnectionSettingsType = ConnectionSettingsType.Bluetooth;
            connectionSettingsTask.Show();
            UpdateToggleButtons();
        }

        private void AirplaneSwitch_Click(object sender, RoutedEventArgs e)
        {
            ConnectionSettingsTask connectionSettingsTask = new ConnectionSettingsTask();
            connectionSettingsTask.ConnectionSettingsType = ConnectionSettingsType.AirplaneMode;
            connectionSettingsTask.Show();
            UpdateToggleButtons();

        }

        private void GsmDataSwitch_Click(object sender, RoutedEventArgs e)
        {
            ConnectionSettingsTask connectionSettingsTask = new ConnectionSettingsTask();
            connectionSettingsTask.ConnectionSettingsType = ConnectionSettingsType.Cellular;
            connectionSettingsTask.Show();
            UpdateToggleButtons();
        }

        public bool IsAirplaneMode()
        {
            bool[] networks = new bool[4] { DeviceNetworkInformation.IsNetworkAvailable, DeviceNetworkInformation.IsCellularDataEnabled, DeviceNetworkInformation.IsCellularDataRoamingEnabled, DeviceNetworkInformation.IsWiFiEnabled };
            return (networks.Count(n => n) < 1);
        }

        private async Task<bool> IsBluetoothOn()
        {
            PeerFinder.AlternateIdentities["Bluetooth:Paired"] = "";
            try
            {
                var peers = await PeerFinder.FindAllPeersAsync();
                return true;
            }
            catch (Exception ex)
            {
                if ((uint)ex.HResult == 0x8007048F)
                {
                    return false;
                }
                throw;
            }
        }

        public async void UpdateToggleButtons()
        {
            AirplaneSwitch.IsChecked = IsAirplaneMode();
            WiFiSwitch.IsChecked = DeviceNetworkInformation.IsWiFiEnabled;
            GsmDataSwitch.IsChecked = DeviceNetworkInformation.IsCellularDataEnabled;
            bool bluetooth = await IsBluetoothOn();
            BluetoothSwitch.IsChecked = bluetooth;
            if (!bluetooth)
            {
                BluetoothSearchSwitch.IsChecked = false;
                BluetoothSearchSwitch.IsEnabled = false;

                BluetoothListenSwitch.IsChecked = false;
                BluetoothListenSwitch.IsEnabled = false;
            }
            else
            {
                BluetoothSearchSwitch.IsEnabled = true;
                BluetoothListenSwitch.IsEnabled = true;
            }
            GpsSwitch.IsChecked = GpsRunning;
        }

        GeoCoordinateWatcher GPS = new GeoCoordinateWatcher(GeoPositionAccuracy.High);
        private bool GpsRunning ;

        private void TurnOffGPS()
        {
            GPS.Stop();
            GpsRunning = false;
        }

        private void TurnOnGPS()
        {
            GPS.Start();
            GpsRunning = true;
        }

        private void PhoneApplicationPage_Loaded(object sender, RoutedEventArgs e)
        {
            UpdateToggleButtons();
            PhoneApplicationService.Current.ApplicationIdleDetectionMode = IdleDetectionMode.Disabled;
        }

        private DispatcherTimer timer;
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            if (timer == null)
            {
                timer = new DispatcherTimer();
                timer.Interval = TimeSpan.FromSeconds(1);
                timer.Tick += OnTimerTick;
                timer.Start();
            }
            BackgroundAudioPlayer.Instance.Stop();
            base.OnNavigatedFrom(e);
        }

        void OnTimerTick(Object sender, EventArgs args)
        {
            UpdateToggleButtons();
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            if (timer != null)
            {
                timer.Stop();
                timer = null;
            }

            if(BackgroundAlarmSwitch.IsChecked==true)
                BackgroundAudioPlayer.Instance.Play();

            base.OnNavigatedFrom(e);
        }

        private void GpsSwitch_Checked(object sender, RoutedEventArgs e)
        {
            TurnOnGPS();
        }

        private void GpsSwitch_Unchecked(object sender, RoutedEventArgs e)
        {
            TurnOffGPS();
        }

        private async void BluetoothSearchSwitch_Checked(object sender, RoutedEventArgs e)
        {
            try
            {
                PeerFinder.Start();
                while (BluetoothSwitch.IsChecked == true)
                {
                   // PeerFinder.
                    await PeerFinder.FindAllPeersAsync();
                }
            }
            catch (Exception)
            {
            }
            finally
            {
                PeerFinder.Stop();
            }
        }
    }
}